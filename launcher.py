from safety import token
from gutils import *
from error_handleing import errorHandler

def _prefix_callable(bot, msg):
    user_id = bot.user.id
    base = [f'<@!{user_id}> ', f'<@{user_id}> ']
    default = [',']
    if msg.guild is None:
        base.extend(default)
    else:
        base.extend(bot.prefixes.get(msg.guild.id, default))
    return base

initial_extensions = []
for i in os.listdir('cogs'):
    if '.py' in i:
        i = i.split('.py',1)[0]
        initial_extensions.append(f'cogs.{i}')


class BotClient(commands.AutoShardedBot):
    def __init__(self):
        super().__init__(command_prefix = _prefix_callable,description='Best game bot on all of Discord.'
                                                                       '', help_attrs=dict(hidden=True))

        for extension in initial_extensions:
            try:
                self.load_extension(extension)
                print(f'Loaded extension {extension}')
            except Exception as e:
                print(f'Failed to load extension {extension}')
                traceback.print_exc()

        self.prefixes = {}
        self.internal = {}

    def uptime(self):
        return timenow(utc=True) - self.uptime_

    async def on_ready(self):
        if not hasattr(self, 'uptime_'):
            self.uptime_ = datetime.datetime.utcnow()
        print(f'Ready: {self.user} (ID: {self.user.id})')

    async def on_resumed(self):
        print('resumed...')

    async def on_command_error(self, ctx, error):
        await errorHandler(self, ctx, error)

    async def on_message(self, message):
        if message.author.bot:
            return
        #await self.ban(message.author) # for later

        await self.process_commands(message)

    async def close(self):
        await super().close()

    def run(self):
        super().run(token, reconnect=True)

bot = BotClient()

async def playing_status():
    statuses = [
        ['watching',    'online',   'out for good games'],
        ['playing',     'online',   ',help'],
        ['listening',   'dnd',      'people\'s keyboards making code'],
        ['streaming',   'online',   'cats live'],
    ]
    await bot.wait_until_ready()
    counter = 0
    while not bot.is_closed():
        mode, status, name = statuses[counter]
        mode = getattr(discord.ActivityType, mode)
        status = getattr(discord.Status, status)
        await bot.change_presence(activity=discord.Activity(name=name, type=mode, url='https://www.twitch.tv/#'), status=status)
        counter += 1
        if counter == len(statuses):
            counter = 0
        await asyncio.sleep(300)


## -=| START POSTGRES CONNECTION |=- ##
# async def psqlStart(uri, **kwargs):
#     def _encode_jsonb(value):
#         return json.dumps(value)

#     def _decode_jsonb(value):
#         return json.loads(value)

#     old_init = kwargs.pop('init', None)

#     async def init(con):
#         await con.set_type_codec('jsonb', schema='pg_catalog', encoder=_encode_jsonb, decoder=_decode_jsonb, format='text')
#         if old_init is not None:
#             await old_init(con)

#     pool = await asyncpg.create_pool(uri, init=init, **kwargs)
#     return pool

async def run():
    # await psqlStart(config.postgresql, command_timeout=60)
    return None

## -=| START BOT |=- ##

loop = asyncio.get_event_loop()
try:
    pool = loop.run_until_complete(run())
except Exception as e:
    print(e)
    print('Could not set up PostgreSQL',fmt='ERROR')
    input('>>')

bot.loop = loop
bot.loop.create_task(playing_status())
# bot.pool = pool
# bot.sq = sQ(pool=pool) # will send later
bot.run()
