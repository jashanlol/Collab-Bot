from gutils import *
import cogs
from cogs.utils.text import *







async def cmds_MissingRequiredArgument(self,ctx,error):
    await ctx.send(f'Missing args: {error}')

async def cmds_CommandInvokeError(self, ctx, error):
    print(join('\n', traceback.format_tb(error.original.__traceback__)),fmt='ERROR', end='\n', color=True, printColor='r')
    await ctx.send(f'```py\n{error.original.__class__.__name__}: {error.original}```')

async def cmds_intermediate_gameerror(self, ctx, error):
    await ctx.send(f'```py\n❌  {error.args[0]}```',delete_after=30)












async def errorHandler(self,ctx,error):
    errorFuncDict = {
        commands.errors.MissingRequiredArgument: cmds_MissingRequiredArgument,
        commands.CommandInvokeError: cmds_CommandInvokeError,
        cogs.intermediate.GameError: cmds_intermediate_gameerror,
    }

    errorFunc = errorFuncDict.get(type(error))
    if errorFunc:
        await errorFunc(self,ctx,error)
        return

    print(type(error), error)
    await ctx.send(f'{type(error)}  {error}')
