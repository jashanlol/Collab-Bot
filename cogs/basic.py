import random

from discord.ext import commands


class Basic:
    """Basic games.


    Easy to play and take less than 15 seconds!"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def rps(self, ctx, choice: str.lower):
        """See if you can beat the bot in rock paper scissors.


        Choices: rock, r, paper, p, scissors, s


        Ex. ,rps paper"""
        rand = random.randint(0, 2)
        choice_list = ['paper', 'scissors', 'rock']
        if choice not in choice_list:
            choice_list = ['p', 's', 'r']
            if choice not in choice_list:
                return await ctx.send('Please choose rock, paper, or scissors.')
        outcomes = ['draw', 'lose', 'win']
        emojis = ['<:paper:452608000369229824>', '<:scissors:452610342996934667>', '<:rock:452606769332486144>']
        result = (choice_list.index(choice) + rand) % 3
        g = outcomes[rand]
        await ctx.send(f'`[{ctx.author}]` I picked {emojis[result]} you `{g}`.')

    @commands.command()
    async def flip(self, ctx, choice: str.lower):
        """Flip a coin any coin.


        Choices: heads, h, tails, t


        Ex. ,flip t"""
        if choice not in ['h', 'heads', 't', 'tails']:
            return await ctx.send('Please pick heads or tails.')
        rand = random.randint(0, 1)
        choice_list = ['heads', 'tails']
        if choice not in choice_list:
            choice_list = ['h', 't']
        result = (choice_list.index(choice) + rand) % 2
        g = 'False'
        if choice_list[result] == choice: g = 'True'
        await ctx.send(f'`[{ctx.author}]` `{g}`')

    @commands.command(name='8ball')
    async def _ball(self, ctx):
        """Need an answer? You came to the right place.


        Nothing more I can help you with besides telling you the truth.


        Ex. ,8ball Am I a good developer?"""
        choice = random.choice(['Yes', 'No', 'Maybe'])
        await ctx.send(f'`[{ctx.author}]` `{choice}`.')

    @commands.command()
    async def choose(self, ctx, *choices: str):
        """Let me choose for you.


        No more help can be given.


        Ex. ,choose pizza burrito"""
        if not choices:
            return await ctx.send('Choices is a required argument that is missing.')
        choice = random.choice(choices)
        await ctx.send(f'`[{ctx.author}]` `{choice}`.')


def setup(bot):
    bot.add_cog(Basic(bot))
