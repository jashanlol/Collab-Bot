import io
import textwrap
import traceback
from contextlib import redirect_stdout

from discord.ext import commands
import discord


class Admin:
    """Bot owner only commands."""

    def __init__(self, bot):
        self.bot = bot
        self._last_result = None
        self.sessions = set()

    def __local_check(self, ctx):
        return ctx.author.id in [226121791670583296, 166292704462897154, 294894701708967936, 452617603173842963]

    @commands.command(hidden=True)
    async def load(self, ctx, *, module):
        """Loads a module."""
        try:
            self.bot.load_extension(module)
            await ctx.send('\N{OK HAND SIGN}')
        except:
            e = discord.Embed(description=f'```py\n{traceback.format_exc()}\n```', color=ctx.author.color)
            await ctx.send(embed=e, delete_after=30)

    @commands.command(hidden=True)
    async def unload(self, ctx, *, module):
        """Unloads a module."""
        try:
            self.bot.unload_extension(module)
            await ctx.send('\N{OK HAND SIGN}')
        except:
            e = discord.Embed(description=f'```py\n{traceback.format_exc()}\n```', color=ctx.author.color)
            await ctx.send(embed=e, delete_after=30)

    @commands.command(hidden=True)
    async def reload(self, ctx, *, module):
        try:
            self.bot.unload_extension(module)
            self.bot.load_extension(module)
            await ctx.send('\N{OK HAND SIGN}')
        except:
            e = discord.Embed(description=f'```py\n{traceback.format_exc()}\n```', color=ctx.author.color)
            await ctx.send(embed=e, delete_after=30)

    @commands.command(hidden=True)
    async def shutdown(self, ctx):
        await ctx.send('\N{OK HAND SIGN}')
        await self.bot.logout()

    def cleanup_code(self, content):
        """Automatically removes code blocks from the code."""
        # remove ```py\n```
        if content.startswith('```') and content.endswith('```'):
            return '\n'.join(content.split('\n')[1:-1])

        # remove `foo`
        return content.strip('` \n')

    def get_syntax_error(self, e):
        if e.text is None:
            return f'```py\n{e.__class__.__name__}: {e}\n```'
        return f'```py\n{e.text}{"^":>{e.offset}}\n{e.__class__.__name__}: {e}```'


    @commands.command(pass_context=True, hidden=True, name='eval')
    async def _eval(self, ctx, *, body: str):
        """Evaluates a code"""

        env = {
            'bot': self.bot,
            'ctx': ctx,
            'channel': ctx.channel,
            'author': ctx.author,
            'guild': ctx.guild,
            'message': ctx.message,
            '_': self._last_result
        }

        env.update(globals())

        body = self.cleanup_code(body)
        stdout = io.StringIO()

        to_compile = f'async def func():\n{textwrap.indent(body, "  ")}'

        try:
            exec(to_compile, env)
        except Exception as e:
            return await ctx.send(f'```py\n{e.__class__.__name__}: {e}\n```')

        func = env['func']
        try:
            with redirect_stdout(stdout):
                ret = await func()
        except Exception as e:
            value = stdout.getvalue()
            await ctx.send(f'```py\n{value}{traceback.format_exc()}\n```')
        else:
            value = stdout.getvalue()
            try:
                await ctx.message.add_reaction('\N{OK HAND SIGN}')
            except:
                pass

            if ret is None:
                if value:
                    await ctx.send(f'```py\n{value}\n```')
            else:
                self._last_result = ret
                await ctx.send(f'```py\n{value}{ret}\n```')


def setup(bot):
    bot.add_cog(Admin(bot))

