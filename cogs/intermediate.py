import numpy as np
import discord
import asyncio
from discord.ext import commands
from .utils.text import join


class GameError(commands.CommandError):
    def __init__(self, message, errors = None):
        super().__init__(message)
        self.errors = errors


class BOARD:
    def __init__(self, **kw):
        ## -=| DEFINE VARIABLES |=- ##
        self.users          = kw.get('users'        , 2)
        self.shape          = kw.get('shape'        , [7, 6])
        self.players        = kw.get('players'      , ['O', 'X'])
        self.player_names   = kw.get('player_names' , ['Test user 1', 'The bot'])
        self.empty          = kw.get('empty'        , ' ')
        self.splitCol       = kw.get('splitCol'     , ' | ')
        self.splitRow       = kw.get('splitRow'     , '\n')
        self.splitNumber    = kw.get('splitNumber'  , '   ')
        self.numberHeader   = kw.get('numberHeader' , None)
        self.numberType     = kw.get('numberType'   , None) # set to 'emoji' for emojis
        self.override       = kw.get('override'     , None)
        self.winsize        = kw.get('winsize'      , 4)
        self.msg = None

        ## -=| DEFINE BOARD |=- ##
        self.board = np.zeros(self.shape[::-1])
        if self.override:
            self.board = np.array(self.override)

        self.size_Y = self.board.shape[0]
        self.size_X = self.board.shape[1]
        self.columns = [self.size_Y - 1] * self.size_X

        ## -=| START |=- ##
        self.users = list(range(1, self.users + 1))
        self.turnCounter = 0
        self.turn = self.users[self.turnCounter]

    def nextTurn(self):
        self.turnCounter += 1
        if self.turnCounter == len(self.users):
            self.turnCounter = 0
        self.turn = self.users[self.turnCounter]

    def getTurnPlayer(self):
        return self.players[self.turn - 1], self.player_names[self.turn - 1]

    def add(self,col):
        x = col-1
        if x < 0:
            raise GameError(f'Invalid column please use a number between 1 and {self.size_X}')

        elif col > self.size_X:
            raise GameError(f'Invalid column please use a number between 1 and {self.size_X}')

        y = self.columns[x]
        if y < 0:
            raise GameError('Column is full')

        self.board[y][x] = self.turn
        self.columns[x] -= 1
        self.nextTurn()

    def render(self):
        if len(self.players) != len(self.users):
            raise GameError(f'Player len does not match user len, Expected {len(self.users)} players got {len(self.players)}.')
        users = [0]+self.users # creates [0,1,2]

        output = []

        each = None
        if self.numberType == 'emoji':
            each = '{0}%s' % chr(8419)
        output.append(join(self.splitNumber, list(range(1, self.size_X + 1)),each=each))

        if self.numberHeader:
            output.append(self.numberHeader*len(output[0]))

        for y in range(self.size_Y):
            row = []
            for x in range(self.size_X):
                item = self.board[y][x]
                idx = users.index(item)
                if idx == 0:
                    row.append(self.empty)
                else:
                    row.append(self.players[idx-1])
            output.append(self.splitCol.join(row))
        output = self.splitRow.join(output)
        return(output)

    def check_wins(self,vals):
        run = []
        expect = None
        for v in vals:
            if (v == expect) or (expect is None):
                run.append(v)
                if len(run) >= self.winsize and int(v) != 0:
                    return(v)
            else:
                run = [v]
            expect = v

    def check(self):
        board = self.board
        items = []

        for x in range(self.size_X):
            items.append(board[:, x])
        for y in range(self.size_Y):
            items.append(board[y, :])

        x, y = board.shape
        diags = [board[::-1,:].diagonal(i) for i in range(-board.shape[0]+1,board.shape[1])] # checks diagonally
        diags.extend(board.diagonal(i) for i in range(board.shape[1]-1,-board.shape[0],-1))
        items.extend([n.tolist() for n in diags])

        for i in items:
            win = self.check_wins(i)
            if win:
                return self.players[int(win) - 1], self.player_names[int(win) - 1]



class testing:
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.games = {}
        self.messages = {}

    @commands.command()
    async def new(self, ctx, gameid):
        self.games[gameid] = BOARD(
            users       = 2,
            size        = [7, 6],
            players     = ['🔵', '🔴'],
            empty       = '⚫',
            splitCol    = '',
            splitRow    = '\n',
            splitNumber = '',
            numberType  = 'emoji')
        self.messages[gameid] = []
        msg = await ctx.send(f'Ready use `play {gameid} [col]`')
        self.messages[gameid].append(ctx.message)
        self.messages[gameid].append(msg)

    @commands.command()
    async def play(self, ctx, gameid, col: int):
        board = self.games.get(gameid)
        if not board:
            await ctx.send('Invalid board')
            return

        board.add(col)

        rendered_board = board.render()
        user, username = board.getTurnPlayer()
        embed = discord.Embed(description=f'`[{username}\'s Turn "{user}"]` `Last Move: {col}`\n{rendered_board}', color=0x36393E)
        embed2 = discord.Embed(title=f'[{username}] as "{user}" Won!',description=f'{rendered_board}',
                              color=0x36393E)
        async def checkmsg(msg):
            messages = []
            for m in await msg.channel.history(limit=10).flatten():
                messages.append(m.id)
            if msg.id in messages:
                return True
            else:
                return False

        if board.msg:
            if await checkmsg(board.msg) is True:
                await board.msg.edit(embed=embed)
            else:
                await board.msg.delete()
                board.msg = await ctx.send(embed=embed)
        else:
            board.msg = await ctx.send(embed=embed)
        self.messages[gameid].append(ctx.message)

        winner = board.check()
        if winner:
            await board.msg.edit(embed=embed2)
            await ctx.channel.delete_messages(self.messages[gameid])
            del self.games[gameid]


    @commands.command()
    async def quit(self, ctx, gameid):
        board = self.games.get(gameid)
        if board.msg:
            await board.msg.delete()
            await ctx.message.delete()
            await ctx.channel.delete_messages(self.messages[gameid])
            del self.games[gameid]
        else:
            await ctx.send('Invalid Board!', delete_after=10)


def setup(bot: commands.Bot):
    bot.add_cog(testing(bot))
