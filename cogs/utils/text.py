def listJoin(join, inp, each=None):
    if not inp:
        return ('')
    i = []
    for item in inp:
        if not type(item) is str:
            item = str(item)
        if each:
            item = each.format(item)
        i.append(item)
    inp = i
    if len(inp) == 1:
        return (str(inp[0]))

    if join == ',and':
        opt1 = inp[:-1]
        if len(opt1) == 1:
            opt1 = str(opt1[0])
        else:
            opt1 = ', '.join(opt1)

        opt2 = inp[-1]
        return '%s and %s' % (opt1, opt2)
    elif join == ',or':
        opt1 = inp[:-1]
        if len(opt1) == 1:
            opt1 = str(opt1[0])
        else:
            opt1 = ', '.join(opt1)

        opt2 = inp[-1]
        return '%s or %s' % (opt1, opt2)
    else:
        return (join.join(inp))
join = listJoin