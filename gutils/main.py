from gutils.imports import *

class Color_:
    def __init__(self):
        self.frep = {'bl':30,'r':31,'g':32,'y':33,'b':34,'m':35,'c':36,'w':37}
        self.brep = {'bl':40,'r':41,'g':42,'y':43,'b':44,'m':45,'c':46,'w':47}
        self.cmd = {'reset':'0m'}
    def t(self):
        for c_ in range(30,37+1):
            for v_ in range(40, 47 + 1):
                for m in range(0, 8 + 1):
                    builtins.print('\x1b[{0};{1};{2}m{0};{1};{2}\x1b[0m'.format(m, c_, v_),end=' ')
                builtins.print('\n')

        builtins.print('\x1b[6;30;42m' + 'Success!' + '\x1b[0m')
    def color(self,args):
        att = None
        fore = None
        back = None
        cmd = None
        if type(args) is list:
            att,fore,back = args
        else:
            cmd = args
        if fore:
            if type(fore) is str:
                fore = self.frep.get(fore)
            if type(back) is str:
                back = self.brep.get(back)
            return('\x1b[%s;%s;%sm'%(att,fore,back))
        if cmd:
            return('\x1b[%s' % (self.cmd.get(cmd)))

    def c(self,inp):
        if '||[]||' in inp:
            inp = inp.replace('||[]||', '')
        if not 'linux' in str(sys.platform):
            inp = re.sub('\|\|\[([a-z0-9\;]+)\]\|\|', '', inp).strip()
        else:
            match = re.findall(r'''\|\|\[([a-z0-9\;]+)\]\|\|''', inp, flags=re.IGNORECASE)
            done = []
            if match:
                for i in match:
                    if not i in done:
                        bi = i
                        if ';' in i:
                            i = i.split(';')
                        r = self.color(i)
                        if not r:
                            r = ''
                        inp = inp.replace("||[%s]||"%bi,r)
                        done.append(bi)
        return(inp)

cc = Color_()

def printTS(pc):
    t = '[%d/%m %H:%M:%S {0}]:'
    if 'linux' in str(sys.platform):
        c = {'bl':30,'r':31,'g':32,'y':33,'b':34,'m':35,'c':36,'w':37}.get(pc)
        c1 = '\x1b[0;%s;40m'%(c)
        c2 = '\x1b[0m'
        t = '[{0}%H{1}:{0}%M{1}:{0}%S{1} {2}]:'.format(c1,c2,'{0}')
    return(datetime.datetime.now().strftime(t))

def printFormat(inp, fmt,pc):
    s_ = printTS(pc).format(fmt)
    inpStr = ''
    if type(inp) is list:
        inpStr = '\t'.join(inp)
    else:
        inpStr = inp
    s__ = s_
    if "\n" in inpStr:
        inp_ = inpStr.split("\n")
        c = 0
        for i in inp_:
            if c == 0:
                s_ += " %s" % i
            else:
                s_ += "\n%s %s" % (s__, i)
            c += 1
    else:
        s_ += " %s" % inpStr
    return(s_)

def print(*args, fmt='INFO', end = '\n', color=False, printColor='c'):
    args = list(args)
    args_ = []
    for a in args:
        args_.append(str(a))
    args = args_
    string = ''
    if fmt:
        string = printFormat(args, fmt,pc=printColor)
    else:
        string = '\t'.join(args)
    if color == True:
        string = cc.c(string)

    builtins.print(string,end=end)


def timenow(utc=False, sqlTs=False):
    ts = datetime.datetime.now()
    if utc == True:
        ts = datetime.datetime.utcnow()
    if sqlTs == True:
        f = '%Y-%m-%d %H:%M:%S'
        ts = ts.strftime(f)
    return(ts)